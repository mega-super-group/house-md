# start with the node:alpine image
FROM node:alpine

# setup dependencies and copy app as root
RUN apk update && apk upgrade && apk add git
RUN npm install typescript -g
WORKDIR /service
COPY package.json .
RUN npm install
COPY . .
EXPOSE 8080

CMD ["npm", "run", "stage"]
