import {App} from "alpha-omega";
import {SeedController} from "./controllers/seed/seed.data";

App.init("dist/routes").then(() => {
	
	SeedController.testSeed().then(
		res => {
			App.start(8080);
		}
	);
	
});

