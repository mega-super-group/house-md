import {AuthenticatedRequest, OauthBearerCredentials, Response} from "alpha-omega";
import {HealthCondition} from "../../models/health.condition";

export class HealthConditionsController {
	
	public static async list(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const conditions = await HealthCondition.list<HealthCondition>(1,100);
		
		const out = new Array(conditions.length);
		
		for(let i = 0; i < conditions.length; i++) {
			out[i] = conditions[i].toObject();
		}
		
		return res.json(out);
		
	}
	
	public static async search(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
	
		const text = req.params.text;
		
		const conditions = await HealthCondition.findWhereTextLike(text);
		
		const out = new Array(conditions.length);
		
		for(let i = 0; i < conditions.length; i++) {
			out[i] = conditions[i].toObject();
		}
		
		return res.json(out);
		
	}
	
}