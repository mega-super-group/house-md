import * as CSV from "fast-csv";
import * as FS from "fs";
import {SeedUsers} from "./seed.users";
import {User} from "../../models/user";
import {Diagnosis} from "../../models/diagnosis";
import {HealthCondition} from "../../models/health.condition";
import {App} from "alpha-omega";
import {Collection} from "mongodb";

export class SeedController {
	
	public static async testSeed() {
		
		const diagnosis = await Diagnosis.list(1,100);
		
		if(diagnosis.length === 0) {
			await SeedController.seedAll();
		}
		
	}
	
	public static async seedAll() {
		
		App.log("Seeding started", "seed:session");
		
		await SeedUsers.seedUsers();
		await SeedController.seedConditions();
		await SeedController.seedCases();
		
		const hcStruct = HealthCondition.__getStructureStatic(HealthCondition);
		const conn = hcStruct.connection as Collection;
		
		await conn.createIndex({code: "text", description: "text"});
		
	}
	
	public static async seedConditions() {
		
		const data = await SeedController.readCSV("imports/conditions.csv");
		
		for(const desc of data) {
			
			const cond = new HealthCondition();
			cond.code = desc.ident;
			cond.description = desc.desc;
			
			await cond.save();
			
		}
		
	}
	
	public static async seedCases() {
	
		const data = await SeedController.readDirectory("imports/cases");
		
		const users = await User.list<User>(1, 1000);
		
		for(const desc of data) {
			
			const user = users[SeedUsers.nextRandom(0, users.length-1)];
			
			const diagnose = new Diagnosis();
			diagnose.description = desc;
			diagnose.user = user;
			
			await diagnose.save();
			
		}
		
	}
	
	public static async readCSV(path: string) : Promise<ConditionTuple[]> {
		
		return new Promise<ConditionTuple[]>((res, rej) => {
			
			const out: ConditionTuple[] = [];
			
			CSV.fromPath(path, {delimiter: "\t"})
				.on("data", (data) => {
				
				if(data.length > 1) {
					out.push(new ConditionTuple(data[0], data[1]));
				}
				
			}).on("end", () => {
				
					res(out);
					
			}).on("data-invalid", (e) => {
				
				console.error(e);
				
			});
			
		})
		
	}
	
	public static async readDirectory(path: string) : Promise<string[]> {
		
		return new Promise<string[]>((resolve, reject) => {
			
			FS.readdir(path, (err, filenames) => {
				
				if (err)
					return reject(err);
				
				Promise.all<string>(filenames.map(filename => {
					
					return new Promise((res, rej) => {
						
						const fullPath =  path + '/' + filename;
						
						FS.readFile(fullPath, "utf-8", (err, content: string) => {
							
							if (err)
								return rej(err);
							
							res(content);
							
						})
						
					})
					
				})).then((data) => {
					
					resolve(data);
					
				});
				
			});
			
		});
		
	}
	
}

class ConditionTuple {
	public ident: string;
	public desc: string;
	constructor(a: string, b: string) {
		this.ident = a;
		this.desc = b;
	}
}