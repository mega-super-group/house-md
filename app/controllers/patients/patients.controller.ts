import {AuthenticatedRequest, OauthBearerCredentials, Response} from "alpha-omega";
import {User} from "../../models/user";
import {Diagnosis, DiagnosisStatus} from "../../models/diagnosis";
import {HealthCondition} from "../../models/health.condition";
import {DataOutputOrderType} from "alpha-omega/dist/orm/common/model";

export class PatientsController {
	
	/**
	 * Output next diagnosis for authorized doctor
	 *
	 * @param req
	 * @param res
	 */
	public static async nextDiagnosis(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
	
		const userId = req.auth.data.user;
		
		const user = await User.find<User>(Number(userId));
		
		if(user.isDoctor) {
		
			/*
				Check current doctor pending records
			 */
			const doctor = await user.doctorate;
			
			const pending = await doctor.paginate(1)
				.seek()
				.where("status")
				.lesser(DiagnosisStatus.SIGNED)
				.among().diagnosisRecords;
			
			if(pending.length > 0) {
				
				await pending[0].user;
				return res.json(pending[0].toObject());
				
			}
			
			/*
				If Doctor has no records to check on then get new from the queue
			 */
			const diagnose = await Diagnosis.getOneNonSigned();
			
			if(diagnose === null) {
				return res.status(404)
					.send("Everybody healthy");
			}
			
			if(diagnose.status < DiagnosisStatus.ASSIGNED) {
				
				diagnose.doctors = [doctor];
				diagnose.status = DiagnosisStatus.ASSIGNED;
				
				await diagnose.save();
				
				await diagnose.user;
				
				return res.json(diagnose.toObject());
			}
			
			return res.status(403)
				.send("Cannot alter this resource: status is too high for reassign");
			
		}
		
		return res.status(403)
			.send("Can't access resource");
	
	}
	
	/**
	 * Sign diagnosis by authorized doctor
	 *
	 * @param req
	 * @param res
	 */
	public static async confirmDiagnosis(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const data = req.json as Diagnosis;
		
		if(data === null) {
			return res.status(400)
				.send("Cannot fulfill diagnosis, data is null");
		}
		
		if(typeof data.conditions === "undefined") {
			return res.status(400)
				.send("Cannot fulfill diagnosis, conditions are empty");
		}
		
		if(!Array.isArray(data.conditions)) {
			return res.status(400)
				.send("Conditions should be represented as list");
		}
		
		const userId = req.auth.data.user;
		
		const user = await User.find<User>(Number(userId));
		
		if(!user.isDoctor) {
			return res.status(403)
				.send("Only Doctor can confirm diagnosis");
		}
		
		const doctor = await user.doctorate;
		
		const diagnosis = await Diagnosis.find<Diagnosis>(Number(req.params.id));
		
		if(diagnosis.status >= DiagnosisStatus.SIGNED) {
			await diagnosis.signedBy;
			await diagnosis.doctors;
			await diagnosis.conditions;
			return res.json(diagnosis.toObject());
		}
		
		const doctors = await diagnosis.doctors;
		
		const search = doctors.filter(d => doctor.id === d.id);
		
		if(search.length === 0) {
			return res.status(403)
				.send("Only assigned Doctor can confirm diagnosis");
		}
		
		/*
			Check on health conditions
		 */
		const conditions: HealthCondition[] = [];
		
		for(const cond of data.conditions) {
			const condition = await HealthCondition.find(Number(cond.id));
			conditions.push(condition);
		}
		
		if(conditions.length === 0) {
			return res.status(403)
				.send("To confirm diagnosis some conditions should be applied");
		}
		
		/*
			Make diagnosis signed and remove it from doctors queue
		 */
		diagnosis.conditions = conditions;
		diagnosis.status = DiagnosisStatus.SIGNED;
		
		diagnosis.signedBy = doctor;
		
		await diagnosis.save();
		
		await diagnosis.signedBy;
		await diagnosis.conditions;
		
		return res.json(diagnosis.toObject());
		
	}
	
	/**
	 * Output next diagnosis for authorized doctor
	 *
	 * @param req
	 * @param res
	 */
	public static async listDiagnosis(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
		
		const userId = req.auth.data.user;
		
		const user = await User.find<User>(Number(userId));
		
		if(user.isDoctor) {
			
			/*
				Check current doctor signed records
			 */
			const doctor = await user.doctorate;
			
			const signed = await doctor.paginate(10)
				.orderBy([
					{
						name: "createdAt",
						order: DataOutputOrderType.DESC
					}])
				.seek()
				.where("status")
				.greater(DiagnosisStatus.ASSIGNED)
				.among().diagnosisRecords;
			
			if(signed.length > 0) {
				
				await signed[0].user;
				
				const out = [];
				
				for(const o of signed) {
					await o.user;
					await o.conditions;
					out.push(o.toObject());
				}
				
				return res.json(out);
				
			}
			
			return res.status(403)
				.send("Cannot alter this resource: status is too high for reassign");
			
		}
		
		return res.status(403)
			.send("Can't access resource");
		
	}
	
}