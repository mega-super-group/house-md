import {Request, Response} from "alpha-omega";
import {App} from "alpha-omega";

export class ErrorController {

    public static handle(err: any, req: Request, res: Response, next?: Function): Response {

        /*
         *  Check if error is identified as Exception
         */
        if(err.hasOwnProperty("code")) {

            App.error("Error happened in application: " + err.message);

            return res.status(err.code).send(err.message);

        }

        /*
         * Process with unidentified error
         */
        App.error("Error happened in application, and error is of unidentified type");

        App.error(err);

        return res.status(err.code || 500).send(err.message);

    }

}