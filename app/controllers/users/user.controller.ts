import {AuthenticatedRequest, OauthBearerCredentials, Request, Response} from "alpha-omega";
import {Doctor} from "../../models/doctor";
import {User} from "../../models/user";
import {Credentials} from "../../models/credentials";
import {AccessKey} from "../../models/access.key";
import {CommonCredentialsInterface} from "../../interfaces/common.credentials.interface";
import {DataOutputOrderType} from "alpha-omega/dist/orm/common/model";
import {Exception} from "ts-exceptions";

export class UserController {
	
	/**
	 * Will create new User as Doctor
	 *
	 * @param req
	 * @param res
	 */
	public static async createAsDoctor(req: Request, res: Response) {
	
		if(req.json) {
			
			const data = req.json as User;
			
			if(typeof data.doctorate === "undefined") {
				return res.status(400).send("Doctor data should be defined for this action");
			}
			
			if(typeof data.credentials === "undefined") {
				return res.status(400).send("Credentials data should be defined for this action");
			}
			
			/*
				Create new user
			 */
			const user = new User().fromObject(data);
			
			if(user.email.length < 3 && user.first.length === 0 && user.last.length === 0 && user.name.length === 0) {
				return res.status(400).send("User parameters should be defined as non empty");
			}
			
			await user.save();
			
			try {
				
				/*
					Assign doctor record
			    */
				
				const doctor = new Doctor().fromObject(data.doctorate);
				doctor.licenceId = data.doctorate.licenceId;
				doctor.user = user;
				
				if(doctor.licenceId.length === 0) {
					throw new Exception("Licence id should not be empty", 400);
				}
				
				await doctor.save();
				
				/*
					Update User that he is actually a doctor
			    */
				user.isDoctor = true;
				await user.save();
				
				/*
					Create credentials for Password access
			    */
				const creds = await Credentials.createForUser(data.credentials.password, user);
				await creds.save();
				
			} catch (e) {
				
				await user.delete();
				return res.status(400).json(e);
				
			}
			
			/*
				Create key information
			 */
			await AccessKey.createForUser(user);
			await user.paginate(1).accessKeys;
			
			return res.json(user.toObject());
			
		}
		
		return res.status(400).send("Bad data");
	
	}
	
	/**
	 * Will authorize some user credentials
	 *
	 * @param req
	 * @param res
	 */
	public static async authorize(req: Request, res: Response) {
		
		const data = req.json as CommonCredentialsInterface;
		
		if(req.json) {
		
			if(typeof data.password === "undefined" || typeof data.email === "undefined") {
				return res.status(400).send("Credentials data should be defined for this action");
			}
			
			const user = await User.query<User>().construct().where("email").equals(data.email).getOne();
		
			if(user) {
				
				const creds = await user.credentials;
				
				if(!await creds.checkPassword(data.password)) {
					res.status(401).send("Wrong password");
				}
				
				/*
					Create key information
			    */
				const key = await AccessKey.createForUser(user);

				await user.paginate(1)
					.orderBy([
						{
							name: "expiresAt",
							order: DataOutputOrderType.DESC
						}]
					).accessKeys;
				
				return res.json(user.toObject());
				
			}
			
			return res.status(404).send("User not found");
			
		}
		
		return res.status(400).send("Bad data");
		
	}
	
	/**
	 * Will expire user authentication
	 *
	 * @param req
	 * @param res
	 */
	public static async deauthorize(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
	
		const userId = req.auth.data.user;
		const key = req.auth.data.token;
		
		const user = await User.find<User>(Number(userId));
		
		try {
			
			const token = await AccessKey.find<AccessKey>(key);
			
			if(user !== null) {
				token.expiresAt = 0;
				await token.save();
			}
			
		} catch (e) {
		
		}
		
		return res.send();
	
	}
	
	public static async get(req: AuthenticatedRequest<OauthBearerCredentials>, res: Response) {
	
	
	
	}
	
}