import {AccessKey} from "../../models/access.key";
import {User} from "../../models/user";

import {App} from "alpha-omega";
import {Diagnosis} from "../../models/diagnosis";
import {HealthCondition} from "../../models/health.condition";
import {Credentials} from "../../models/credentials";
import {Doctor} from "../../models/doctor";

export class SystemCommands {
	
	public static async truncateAll() {
		
		await AccessKey.truncate();
		await User.truncate();
		await Diagnosis.truncate();
		await HealthCondition.truncate();
		await Credentials.truncate();
		await Doctor.truncate();
		
		App.log("Truncation end", "SYSTEM:TRUNCATE");
		
	}
	
}