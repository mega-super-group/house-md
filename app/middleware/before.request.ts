import {App} from "alpha-omega";
import {Request, Response} from "alpha-omega";

export class BeforeRequest {

    public static handle(req: Request, res: Response, next: Function) : any {

        App.log("Request start: " + req.url + " route","HTTP::" + req.method);

        res.setHeader("Access-Control-Allow-Origin", '*');
        res.setHeader("Access-Control-Allow-Credentials", '*');
        res.setHeader("Access-Control-Allow-Methods", 'GET,POST,PUT,DELETE,PATCH,UPDATE');
        res.setHeader("Access-Control-Allow-Headers",
            "upgrade,authorization,content-type,content-size,user-agent, connection");

        if(req.method === "OPTIONS") {
            return res.sendStatus(200);
        }

        next();

    }

}