import {App, AuthenticatedRequest, AuthType, OauthBearerCredentials, Response} from "alpha-omega";
import {AuthTokenType} from "driver-connector";
import {AccessKey} from "../models/access.key";
import {Exception} from "ts-exceptions";

export class AuthRequest {

    public static async handle(req: AuthenticatedRequest<OauthBearerCredentials>,
                               res: Response,
                               next: Function) {

        App.log("Authorization start: " + req.url + " route","HTTP::" + req.method);
	
	    const auth = req.header("Authorization");
	
	    try {
		
		    if (typeof auth !== "undefined") {
			    
		        const tokenObj = AuthRequest.extractToken(auth);
		        
		        const token = await AccessKey.find<AccessKey>(tokenObj.token);
		        
		        const user = await token.user;
		        
		        if(token.expiresAt < Date.now()) {
		        	throw new Exception("Token expired", 403);
		        }
		        
		        req.auth = {
		            data: {
			            user: String(user.id),
                        application: null,
			            token: token.key,
			            scope: "*",
			            expires: new Date(token.expiresAt).getTime()
                    },
			        authorized: true,
			        type: AuthType.Bearer
                };
                
                return next();
                
		    }
		
		    res.status(401).send();
		
	    } catch (e) {
	        
	        res.status(401).send();
	        
		    throw new Exception("Cannot authorize token: " + e.message, 401);
		
	    }
	    
        return next();

    }
    
    private static extractToken(str: string) {
	
	    let result: AuthToken = {
		    token: null,
		    type: null,
		    client: null,
		    key: null
	    };
	
	    let token: string = "";
	
	    // Start checking if Auth header formed properly with Basic or Bearer
	    if(str.charAt(0) === 'B' || str.charAt(0) === 'b') {
		
		    /*
			 *  Bearer case
			 */
		    if(str.charAt(5) === 'r') {
			
			    // Take rest from the string
			    for(let i=7; i < str.length; i++) {
				    token += str[i];
			    }
			
			    result.type = AuthTokenType.Bearer;
			    result.token = token;
			
			    return result;
			
		    }
		    /*
			 *  Basic case
			 */
		    else if(str.charAt(4) === 'c') {
			
			    // Take rest from the string
			    for(let i=6; i < str.length; i++) {
				    token += str[i];
			    }
			
			    // Continue decoding of Auth Basic
			    const decoded = Buffer.from(token, "base64").toString();
			
			    // Continue decoding of Auth Basic
			    const transformed: Array<string> = decoded.split(':');
			
			    // If both params in place continue
			    if(transformed.length > 1) {
				
				    result.type = AuthTokenType.BasicCredentials;
				    result.token = token;
				    result.client = transformed[0];
				    result.key = transformed[1];
				
				    return result;
				
			    }
			
		    }
		
	    }
	
	    return result;
	
    }

}

export interface AuthToken {
    token: string;
    type: AuthTokenType;
    client: string;
    key: string;
}

