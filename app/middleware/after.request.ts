import {App} from "alpha-omega";
import {Request, Response} from "alpha-omega";

export class AfterRequest {

    public static handle(req: Request, res: Response, next: Function) : void {

        App.log("Request end: " + req.url + " route","HTTP::" + req.method);

    }

}