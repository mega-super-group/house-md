import {App} from "alpha-omega";
import {ParseRequest} from "./middleware/parse.request";
import {AfterRequest} from "./middleware/after.request";
import {NotFoundController} from "./controllers/notfound";
import {ErrorController} from "./controllers/error";
import {BeforeRequest} from "./middleware/before.request";
import {AuthRequest} from "./middleware/auth.request";
import {ManageAuthorization} from "./controllers/auth/manage.authorization";
import {SystemCommands} from "./controllers/cmd/system.commands";
import {SeedController} from "./controllers/seed/seed.data";
import {UserController} from "./controllers/users/user.controller";
import {PatientsController} from "./controllers/patients/patients.controller";
import {HealthConditionsController} from "./controllers/conditions/health.conditions.controller";

export function routeStack() {

    App.router.handleBefore(ParseRequest.handle);
    App.router.handleBefore(BeforeRequest.handle);
    App.router.handleAfter(AfterRequest.handle);
    App.router.handleNotFound(NotFoundController.handle);
    App.router.handleError(ErrorController.handle);

    const auth = AuthRequest.handle;
    
    /* *******************************************************************************
     *
     *                                  API CALLS
     *
     * *******************************************************************************
     */
    
    App.router.get("/api/v1/authorized", ManageAuthorization.checkAuthorized, [auth]);
    
    App.router.post("/api/v1/user/authorize/credentials", UserController.authorize);
    
    App.router.post("/api/v1/user/doctor", UserController.createAsDoctor);
	
    App.router.delete("/api/v1/user/deauthorize", UserController.deauthorize, [auth]);
    
    App.router.get("/api/v1/diagnosis/next", PatientsController.nextDiagnosis, [auth]);
	App.router.put("/api/v1/diagnosis/sign/:id", PatientsController.confirmDiagnosis, [auth]);
	App.router.get("/api/v1/diagnosis/list", PatientsController.listDiagnosis, [auth]);
	
	App.router.get("/api/v1/conditions", HealthConditionsController.list, [auth]);
	App.router.get("/api/v1/conditions/search/:text", HealthConditionsController.search, [auth]);
	
	App.router.command("truncate-all", SystemCommands.truncateAll);
	App.router.command("seed-all", SeedController.seedAll);
	
}