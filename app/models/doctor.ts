import {BelongsToOne, HasMany, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {Diagnosis} from "./diagnosis";
import {User} from "./user";

export class Doctor extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property({unique: true, notNull: true})
	public licenceId: string = null;
	
	@HasMany(() => Diagnosis)
	public diagnosisRecords: Diagnosis[] = [];
	
	@BelongsToOne(() => User)
	public user: User = null;
	
	@HasMany(() => Diagnosis)
	public signedRecords: Diagnosis[] = [];
	
}