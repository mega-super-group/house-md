import {BelongsToOne, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {User} from "./user";
import {SafePassword} from "safe-password";

export class Credentials extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int, expose: false})
	public id: number = null;
	
	@Property({expose: false})
	public password: string = null;
	
	@BelongsToOne(() => User)
	public user: User = null;
	
	public async checkPassword(password: string) : Promise<boolean> {
		
		const safePassword = new SafePassword();
		
		return await safePassword.standard().checkAuto(password, this.password);
		
	}
	
	public static async createForUser(password: string, user: User) {
		
		const safePassword = new SafePassword();
		
		const cred = new Credentials();
		cred.user = user;
		cred.password = await safePassword.standard().encodeAuto(password);
		
		return cred;
		
	}
	
}