import {BelongsToOne, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {User} from "./user";
import * as UUID from "uuid";
import * as Crypto from "crypto";

export class AccessKey extends MongoModel {
	
	@Property({primary: true, protect: true})
	public key: string = null;
	
	@Property()
	public createdAt: Date = new Date();
	
	@Property({index: true})
	public expiresAt: number = 0;
	
	@BelongsToOne(()=>User)
	public user: User = null;
	
	public static async createForUser(user: User): Promise<AccessKey> {
	
		const secret = Crypto.createHash("md5").update(UUID.v4()).digest("hex");
	
		const ac = new AccessKey();
		ac.key = secret;
		ac.expiresAt = new Date(ac.createdAt).getTime() + (1000 * 86400);
		ac.user = user;
		
		await ac.save();
		
		return ac;
		
	}
	
}