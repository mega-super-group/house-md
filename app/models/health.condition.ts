import {BelongsToMany, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {Diagnosis} from "./diagnosis";

export class HealthCondition extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property({unique: true, notNull: true})
	public code: string = null;
	
	@Property()
	public description: string = "";
	
	@Property()
	public spec1: string = "";
	
	@Property()
	public spec2: string = "";
	
	@Property()
	public spec3: string = "";
	
	@BelongsToMany(() => Diagnosis, {weak: true})
	public diagnosis: Diagnosis[] = [];
	
	public static async findWhereTextLike(text: string) : Promise<HealthCondition[]> {
		
		return this.query<HealthCondition>()
			.raw({query: {$text: {$search: text}}})
			.getMany<HealthCondition>();
		
	}
	
}