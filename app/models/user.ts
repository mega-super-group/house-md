import {HasMany, HasOne, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {AccessKey} from "./access.key";
import {Type} from "alpha-omega";
import {Credentials} from "./credentials";
import {Diagnosis} from "./diagnosis";
import {Doctor} from "./doctor";

export class User extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property({index: true, unique: true})
	public email: string = null;
	
	@Property({index: true})
	public name: string = null;
	
	@Property()
	public first: string = null;
	
	@Property()
	public last: string = null;
	
	@Property({protect:true})
	public isDoctor: boolean = false;
	
	/*
	 *  Relations
	 */
	
	@HasOne(()=>Doctor)
	public doctorate: Doctor = null;
	
	@HasOne(() => Credentials)
	public credentials: Credentials = null;
	
	@HasMany(()=>AccessKey)
	public accessKeys: AccessKey[] = [];
	
	@HasMany(()=>Diagnosis)
	public diagnosis: Diagnosis[] = [];
	
}