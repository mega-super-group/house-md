import {BelongsToMany, BelongsToOne, HasMany, MongoModel, Property} from "alpha-omega/dist/orm/mongo.model";
import {Type} from "alpha-omega";
import {User} from "./user";
import {HealthCondition} from "./health.condition";
import {Doctor} from "./doctor";

export class Diagnosis extends MongoModel {
	
	@Property({primary: true, auto: true, type: Type.Int})
	public id: number = null;
	
	@Property()
	public description: string = "";
	
	@Property()
	public status: DiagnosisStatus = DiagnosisStatus.PENDING;
	
	@Property()
	public createdAt: Date = new Date();
	
	@HasMany(() => HealthCondition)
	public conditions: HealthCondition[] = [];
	
	@BelongsToOne(() => User)
	public user: User = null;
	
	@BelongsToMany(() => Doctor, {weak: true})
	public doctors: Doctor[] = [];
	
	@BelongsToOne(() => Doctor, {weak: true})
	public signedBy: Doctor = null;
	
	public static async getOneNonSigned() : Promise<Diagnosis>  {
	
		return this.query<Diagnosis>()
			.raw({query: {status: DiagnosisStatus.PENDING}, sort: {createdAt: -1}, limit: 1})
			.getOne<Diagnosis>();
		
	}
	
}

export enum DiagnosisStatus {
	PENDING,
	ASSIGNED,
	INREVIEW,
	SIGNED,
	CLOSED
}